/**
 * @authors TechBuilder edited by Hot Printer
 * @license MIT <https://mit-license.org/>
 *
 * Notes
 * - The rotary encoders prevail over the serial.
 * - The rotary encoders prevail over the Wifi.
 *
 */
#include <Arduino.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "Rotary.h"

#define UVs D5
#define LEDs D6
#define BUTTON D8
#define ROT_T_A D0
#define ROT_T_B D7
// #define ROT_T_SW A1 // no need the SW

// serial communications
#define PROCESS_STATUS 1
#define WORKING_STATUS 2
#define SET_CONFIG 3
#define GET_CONFIG 4
#define START 5
#define STOP -1
#define CONTINUE 0

// serial communications return
#define WAITING 0
#define GENERIC_ERROR -4
#define STOPPED -1
#define ALREADY_WORKING -2
#define NOT_WORKING 'X'
#define FINISHED 1
#define STARTING 3
#define CONFIG_SET 4
#define WORKING 2

// CONFIG
#define MAX_TIME 1200
#define MAX_WAITING_TIME 1000
#define GLOW_DELAY 10 // UVs glow rate (0-200)

#ifndef STASSID
#define STASSID "SSID"
#define STAPSK  "PASSWORD"
#endif

// GLOBALS
ESP8266WebServer server(80);
Rotary rot = Rotary(ROT_T_A, ROT_T_B);

// vars
const char *ssid = STASSID;
const char *password = STAPSK;
float timeLeftBarPercent = 0.00;
float durationSecFloat = 0.00;
float durationSecSelect = 0.00;
int lightCounter = 1023;
int prevLightCounter = 0;
int timeCounter = 0;
int prevTimeCounter = 1;
int brightness = 0;
int brightnessPercent = 0;
int brightnessPWM = 0;
int seconds = 0;
int minutes = 0;
int durationSec = 0;
long operation = 0;
unsigned long previousMillis = 0;
const long interval = 1000;
char timeOutStr[16];
char serialReturnReport[12];
bool exposureLoop = false;
bool configuring = true; // variable to switch configuration

/**
 * Shows the status
 *
 * @example http://exposurebox.local/
 */
void handleRoot() {
    char temp[550];
    snprintf(temp, 400, R"({"status": "T%02d:%02dB%03d"})", minutes, seconds, brightness);
    server.send(200, "application/json", temp);
}

/**
 * Method to handle all the serial communications and generate a response according to it
 */
int serialize(int actualStatus){
    if (Serial.available() > 0) {
        // read the serial and parse the command
        operation = Serial.parseInt();
        switch(operation){
            case PROCESS_STATUS:
                Serial.println(actualStatus);
                break;
            case WORKING_STATUS:
                if (actualStatus == 2){
                    sprintf(serialReturnReport, "T%02d:%02dB%03d", minutes, seconds, brightness);
                    Serial.println(serialReturnReport);
                } else {
                    Serial.println(NOT_WORKING);
                }
                break;
            case SET_CONFIG:
                if (actualStatus == WAITING){
                    // parse the input string within a timeout or cancel
                    unsigned long started_waiting_at = millis();
                    while (Serial.available() < 0) {
                        if (millis() - started_waiting_at > MAX_WAITING_TIME) {
                            // we cant wait more
                            return GENERIC_ERROR;
                        }
                    }

                    String response = Serial.readString(); // read the String and extract all required values
                    String timeStr = response.substring(2, 6);
                    String brightnessStr = response.substring(7);

                    // Expected format T%04dB%03d
                    // Example "T0010B1023"

                    timeCounter = int(timeStr.toInt());
                    lightCounter = int(brightnessStr.toInt());

                    // finally return the config
                    Serial.println(CONFIG_SET);
                } else {
                    Serial.println(ALREADY_WORKING);
                }
                break;
            case GET_CONFIG:
                sprintf(serialReturnReport, "T%02d:%02dB%03d", minutes, seconds, brightness);
                Serial.println(serialReturnReport);
                break;
            case STOP:
                if (actualStatus == 2){
                    // now working, stop it
                    Serial.println(STOPPED);
                    return STOP;
                } else {
                    Serial.println(GENERIC_ERROR);
                }
                break;
            case START:
                Serial.println(STARTING);
                return START;
                break;
            default:
                return CONTINUE;
        }
    }
    return CONTINUE;
}

void expose(){
    durationSecSelect = float(durationSec);
    durationSecFloat = float(durationSec);

    // Set the brightness to the LEDs
    for (int i = brightnessPWM; i >= 0; i--){
        analogWrite(LEDs, i);
        delay(GLOW_DELAY);
    }

    // Set the brightness to the UV
    for (int c = 0; c <= brightnessPWM; c++) {
        analogWrite(UVs, c);
        delay(GLOW_DELAY);
    }
    Serial.println();

    while(exposureLoop==0){
        timeLeftBarPercent = 100 * (durationSecFloat/durationSecSelect);

        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis >= interval){
            previousMillis = currentMillis;

            minutes = durationSec / 60;
            seconds = durationSec % 60;

            Serial.print("Time Left: ");
            sprintf(timeOutStr, "%02d:%02d", minutes, seconds);
            Serial.println(timeOutStr);
            durationSec--;
            durationSecFloat--;
        }

        if(durationSec <= 0){
            // Set the brightness to the UVs
            for (int i = brightnessPWM; i >= 0; i--){
                analogWrite(UVs, i);
                delay(GLOW_DELAY);
            }
            // Set the brightness to the LEDs
            for (int c = 0; c <= brightnessPWM; c++) {
                analogWrite(LEDs, c);
                delay(GLOW_DELAY);
            }

            Serial.println("Exposure Finished");
            Serial.println("Press Button To Start");
            delay(500);

            while(digitalRead(BUTTON) == LOW){
                // wait for a button press or a start serial input
                if (serialize(FINISHED) == START) break;
            }  //Press button to proceed with system restart
            delay(1000);
            return;
        } else if(digitalRead(BUTTON) == HIGH || serialize(WORKING) == STOP){
            // Set the brightness to the UVs
            for (int i = brightnessPWM; i >= 0; i--){
                analogWrite(UVs, i);
                delay(GLOW_DELAY);
            }
            // Set the brightness to the LEDs
            for (int c = 0; c <= brightnessPWM; c++) {
                analogWrite(LEDs, c);
                delay(GLOW_DELAY);
            }
            Serial.println("Cancelling Process!");
            Serial.println("Exposure Finished");
            Serial.println("Press Button To Start");
            while(digitalRead(BUTTON) == LOW){
                // wait for a button press or a start serial input
                if (serialize(FINISHED) == START) break;
            }  //Press button to proceed with system restart
            delay(1000);
            return;
        }
    }
}

void setup() {
    Serial.println("Starting");
    // define I/O
    pinMode(UVs, OUTPUT);
    pinMode(LEDs, OUTPUT);
    pinMode(BUTTON, INPUT);

    // start the rotatory encoders
    rot.begin();

    // set the UVs and LEDs brightness
    analogWrite(LEDs, LOW);
    digitalWrite(UVs, LOW);

    // start serial communication
    Serial.begin(115200);

    for (int i = 0; i < 255; ++i) {
        analogWrite(LEDs, i);
        delay(GLOW_DELAY);
    }

    // set the ESP as an acces point
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    // track this headers

    server.on("/", handleRoot);
    server.begin(); // Start the HTTP Server
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print("Not connected.");
    }

//    IPAddress HTTPS_ServerIP= WiFi.softAPIP(); // Obtain the IP of the Server
//    Serial.print("Server IP is: "); // Print the IP to the monitor window
//    Serial.println(HTTPS_ServerIP);

    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    Serial.println("Ready");
}

void loop() {
    configuring = true;
    prevLightCounter = 0;
    prevTimeCounter = 1201;
    while (true){
        if (serialize(WAITING) == START) {
            expose();
            break;
        }

        // read the rotary encoders
        unsigned char result = rot.process();

        if (result) {
            if (configuring){
                if (result == DIR_CW) { lightCounter += 50; } else { lightCounter -= 50; }
            } else {
                if (result == DIR_CW){ timeCounter++; } else { timeCounter--; }
            }
        }
        if (timeCounter < 0) timeCounter = 0;
        if (timeCounter > MAX_TIME) timeCounter = MAX_TIME;
        if (lightCounter < 0)  lightCounter = 0;
        if (lightCounter > 1023) lightCounter = 1023;

        // set the variables acording to the rotary
        durationSec = timeCounter;
        brightness = lightCounter;
        brightnessPercent = int(map(brightness, 0, 1023, 0, 100));
        brightnessPWM = int(map(brightness, 0, 1023, 0, 255));

        // write the brightness to the pin
        analogWrite(LEDs, brightnessPWM);

        minutes = durationSec / 60;
        seconds = durationSec % 60;

        if (configuring){
            if (prevLightCounter != lightCounter){
                Serial.print("Brightness: ");
                Serial.println(brightnessPercent);
                prevLightCounter = lightCounter;
            }
        } else {
            if (prevTimeCounter != timeCounter){
                Serial.print("Exposure (seconds): ");
                sprintf(timeOutStr, "%02d:%02d", minutes, seconds);
                Serial.println(timeOutStr);
                prevTimeCounter = timeCounter;
            }
        }

        if (digitalRead(BUTTON)==HIGH && configuring){
            configuring = false;
            delay(1000);
        }
        if (digitalRead(BUTTON)==HIGH && !configuring) {
            Serial.println("Starting");
            expose();
            break;
        }
    }
}
